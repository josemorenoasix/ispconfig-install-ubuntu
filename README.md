# ispconfig-install-ubuntu
[![N|Solid](https://www.ispconfig.org/wp-content/themes/ispconfig/images/ispconfig_logo.png)](https://www.ispconfig.org/)

Bash Script para automatizar el proceso de instalación y configuración de ISPConfig en Ubuntu 16.04 

El proceso manual puede ser consultado en:
[The Perfect Server - Ubuntu 16.04 (Xenial Xerus) with Apache, PHP, MySQL, PureFTPD, BIND, Postfix, Dovecot and ISPConfig 3.1](https://www.howtoforge.com/tutorial/perfect-server-ubuntu-16.04-with-apache-php-myqsl-pureftpd-bind-postfix-doveot-and-ispconfig "The Perfect Server - Ubuntu 16.04")
## Proceso de instalación
### Descarga el script y el fichero de configuración...
```sh
bash <(curl -s https://gitlab.com/josemorenoasix/ispconfig-install-ubuntu/raw/master/start.sh)
```
### Edita el fichero de configuracion
```sh
nano install.cfg
```
### Ejecuta el script como root
```sh
sudo bash install.sh
```
### Aqui tienes un video del proceso de instalación, disfrútalo!
> **TIP:** Máximiza la ventana!

[![asciicast](https://asciinema.org/a/185985.png)](https://asciinema.org/a/185985)