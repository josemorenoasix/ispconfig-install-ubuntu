#!/bin/bash

# Colors
grn=$'\e[1;32m'
blu=$'\e[1;34m'
end=$'\e[0m'

echo -n " - Descargando el script de instalacion desde el repositorio online... "
wget -q -O install.sh https://gitlab.com/josemorenoasix/ispconfig-install-ubuntu/raw/master/install.sh > /dev/null
echo "${grn}OK${end}"

echo -n " - Descargando el script el fichero de configuracion desde el repositorio online... "
wget -q -O install.cfg  https://gitlab.com/josemorenoasix/ispconfig-install-ubuntu/raw/master/install.cfg > /dev/null
echo "${grn}OK${end}"

echo    " - A continuacion:" 
echo    "   · Edita el fichero de configuracion ejecutando ${blu}nano install.cfg${end}" 
echo    "   · Ejecuta ${blu}sudo bash install.sh${end}"